

module.exports = {


    async login({ mysql, res, body }){

        var err = '';

        if(!body.email) err = 'Email invalido.';
        if(!body.senha) err = 'Senha invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var campos = " id_usuario, nome, email, id_escola ";
        var sql = "SELECT "+campos+" FROM usuario WHERE email = '"+body.email+"' AND senha = '"+body.senha+"' ";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            res.json({
                status: "ok",
                resultado: rows[0]
            });
        else
            res.json({
                status: "fail",
                resultado: []
            });

    },

    async get({ mysql, res, body }){

        var err = '';

        if(!body.idEscola) err = 'Escola invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var campos = " id_usuario, nome, email, usuario.id_tipo_usuario, tipo_usuario.descricao as tipo ";
        var sql  = " SELECT "+campos+" FROM usuario ";
        sql += " inner join tipo_usuario on tipo_usuario.id_tipo_usuario = usuario.id_tipo_usuario  ";
        sql += " WHERE id_escola = '"+body.idEscola+"' ";

        if (body.tipo)
            sql += " and usuario.id_tipo_usuario in ("+body.tipo+")";

        if (body.id)
            sql += " and usuario.id_usuario = ("+body.id+")";

        if(body.search)
            sql += " and usuario.nome like '%"+body.search+"%' ";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            if (body.id)
                res.json({
                    status: "ok",
                    resultado: rows[0]
                });
            else
                res.json({
                    status: "ok",
                    resultado: rows
                });
        else
            res.json({
                status: "ok",
                resultado: []
            });

    },

    async salvar({ mysql, res, body }){
        var err = '';

        if(!body.idEscola) err           = 'Escola invalida.';
        if(!body.nome) err               = 'Nome invalido.';
        if(!body.email) err              = 'Email invalido.';
        if(!body.id_tipo_usuario) err    = 'Tipo invalido.';
        if(!body.senha && !body.id_usuario) err = 'Senha invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        if (body.id_usuario) {

            var campos = " nome='"+body.nome+"', email='"+body.email+"', id_tipo_usuario='"+body.id_tipo_usuario+"' ";
            if (body.senha) campos += ", senha = '"+body.senha+"' ";
            var sql  = " update usuario set "+campos+" where id_usuario = "+body.id_usuario+" ";


        } else {

            var campos = " id_usuario, id_escola, nome, email, id_tipo_usuario, senha ";
            var valores = " null, '"+body.idEscola+"', '"+body.nome+"', '"+body.email+"', '"+body.id_tipo_usuario+"', '"+body.senha+"' ";
            var sql  = " insert into usuario ("+campos+") values ("+valores+") ";

        }

        var [rows] = await mysql.query(sql);

        if (body.id_usuario)
            res.json({
                status: "ok",
                resultado: rows.affectedRows
            });
        else
            res.json({
                status: "ok",
                resultado: rows.insertId
            });

    },

    async excluir ({ mysql, res, body }){
        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var sql  = " delete from usuario where id_usuario = "+body.id+" ";
        var [rows] = await mysql.query(sql);
        res.json({
            status: "ok",
            resultado: rows.affectedRows
        });
    }


}

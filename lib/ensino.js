

module.exports = {

    async get({ mysql, res, body }){

        var err = '';

        if(!body.idEscola) err = 'Escola invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var campos = " ensino.*, ensino_escola.id_ensino_escola ";
        var sql  = " SELECT "+campos+" FROM ensino_escola ";
        sql += " inner join ensino on ensino.id_ensino = ensino_escola.id_ensino  ";
        sql += " WHERE id_escola = '"+body.idEscola+"' ";

        if (body.id)
            sql += " and ensino_escola.id_ensino_escola = ("+body.id+")";

        if(body.search)
            sql += " and ensino.nome like '%"+body.search+"%' ";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            if (body.id)
                res.json({
                    status: "ok",
                    resultado: rows[0]
                });
            else
                res.json({
                    status: "ok",
                    resultado: rows
                });
        else
            res.json({
                status: "ok",
                resultado: []
            });

    }

}



module.exports = {

    async get({ mysql, res, body }){

        var err = '';

        if(!body.idEscola) err = 'Escola invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

            var campos = " turma.*, ensino.nome as ensino ";
        var sql  = " SELECT "+campos+" FROM turma ";
        sql += " inner join ensino_escola on ensino_escola.id_ensino_escola = turma.id_ensino_escola  ";
        sql += " inner join ensino on ensino.id_ensino = ensino_escola.id_ensino  ";
        sql += " WHERE id_escola = '"+body.idEscola+"' ";

        if (body.id)
            sql += " and turma.id_turma = ("+body.id+")";

        if(body.search)
            sql += " and turma.nome like '%"+body.search+"%' ";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            if (body.id)
                res.json({
                    status: "ok",
                    resultado: rows[0]
                });
            else
                res.json({
                    status: "ok",
                    resultado: rows
                });
        else
            res.json({
                status: "ok",
                resultado: []
            });

    },

    async salvar({ mysql, res, body }){
        var err = '';

        if(!body.idEscola) err           = 'Escola invalida.';
        if(!body.nome) err  = 'Nome invalido.';
        if(!body.sigla) err = 'Sigla invalido.';
        if(!body.nivel) err = 'Nível invalido.';
        if(!body.serie) err = 'Série invalido.';
        if(!body.id_ensino_escola && body.id_ensino_escola != 0)
            err = 'Ensino invalido.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        if (body.id_turma) {

            var campos = " nome='"+body.nome+"', sigla='"+body.sigla+"', nivel='"+body.nivel+"', serie='"+body.serie+"', id_ensino_escola='"+body.id_ensino_escola+"' ";
            var sql  = " update turma set "+campos+" where id_turma = "+body.id_turma+" ";

        } else {

            var campos = " id_turma, nome, sigla, nivel, serie, id_ensino_escola ";
            var valores = " null, '"+body.nome+"', '"+body.sigla+"', '"+body.nivel+"', '"+body.serie+"', '"+body.id_ensino_escola+"' ";
            var sql  = " insert into turma ("+campos+") values ("+valores+") ";

        }

        var [rows] = await mysql.query(sql);

        if (body.id_turma)
            res.json({
                status: "ok",
                resultado: rows.affectedRows
            });
        else
            res.json({
                status: "ok",
                resultado: rows.insertId
            });

    },

    async excluir ({ mysql, res, body }){
        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var sql  = " delete from turma where id_turma = "+body.id+" ";
        var [rows] = await mysql.query(sql);
        res.json({
            status: "ok",
            resultado: rows.affectedRows
        });
    }

}



module.exports = {


    async get({ mysql, res, body }){

        if(!body.idEscola)
            return res.json({
                status: "fail",
                msg: 'Escola invalida.'
            });

        var campos = " usuario.*, aluno_turma.*, turma.nome as turma ";
        var sql  = " SELECT "+campos+" FROM usuario ";
        sql += " INNER JOIN aluno_turma ON aluno_turma.id_usuario = usuario.id_usuario AND aluno_turma.fl_ativo = 'S' ";
        sql += " INNER JOIN turma ON turma.id_turma = aluno_turma.id_turma ";
        sql += " WHERE usuario.id_escola = '"+body.idEscola+"' ";
        sql += "   and usuario.id_tipo_usuario = 5";

        if (body.id)
            sql += " and usuario.id_usuario = "+body.id+" ";

        if (body.idTurma)
            sql += " and aluno_turma.id_turma = "+body.idTurma+" ";

        if(body.search)
            sql += " and aluno_turma.nome like '%"+body.search+"%' ";

        sql += " ORDER BY usuario.nome";

        var [rows] = await mysql.query(sql);

        res.json({
            status: "ok",
            resultado: body.id ? rows[0] : rows
        });
    },

    async salvar({ mysql, res, body }){

        function gerarNumero(digitos) {
          var n = '';
          for (var i = 0; i < digitos; i++) {
            n = n+Math.floor(Math.random() * (9 - 1 + 1) + 1).toString();
          }
          return n;
        }

        var err = '';

        if(!body.nome) err     = 'Nome invalido.';
        if(!body.idEscola) err = 'Escola não encontrada.';
        if(!body.idTurma && !body.id_usuario) err = 'Turma não encontrada.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

            if (body.id_usuario) {

            var campos = " nome='"+body.nome+"', email='"+body.email+"' ";
            var sql  = " update usuario set "+campos+" where id_usuario = "+body.id_usuario+" ";


        } else {

            if (body.email)
                var email = body.email;
            else
                var email = body.nome;

            var senha = gerarNumero(4);  //gerar de forma rantomica

            var campos = " id_usuario, nome, email, senha, id_tipo_usuario, id_escola ";
            var valores = " null, '"+body.nome+"', '"+email+"', '"+senha+"', '5', '"+body.idEscola+"' ";
            var sql  = " insert into usuario ("+campos+") values ("+valores+") ";

        }

        var [rows] = await mysql.query(sql);

        if (body.id_turma)
            res.json({
                status: "ok",
                resultado: rows.affectedRows
            });
        else
            res.json({
                status: "ok",
                resultado: rows.insertId
            });

            //inserir registro de Aluno turma
            var matricula = gerarNumero(8);
            campos  = " id_aluno_turma, id_usuario, id_turma, fl_ativo, matricula ";
            valores = " null, '"+rows.insertId+"', '"+body.idTurma+"', 'S', '"+matricula+"' ";
            sql     = " insert into aluno_turma ("+campos+") values ("+valores+") ";
            var result  = await mysql.query(sql);

    },

    async excluir ({ mysql, res, body }){
        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        //dependencias
        var sql  = " delete from aluno_turma where id_usuario = "+body.id+" ";
        var result = await mysql.query(sql);

        //Tabela principal
        sql  = " delete from usuario where id_usuario = "+body.id+" ";
        var [rows] = await mysql.query(sql);

        res.json({
            status: "ok",
            resultado: rows.affectedRows
        });
    },

    async senha ({ mysql, res, body }){

        function gerarNumero(digitos) {
          var n = '';
          for (var i = 0; i < digitos; i++) {
            n = n+Math.floor(Math.random() * (9 - 1 + 1) + 1).toString();
          }
          return n;
        }

        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var senha = gerarNumero(4);
        var campos = " senha='"+senha+"' ";
        var sql  = " update usuario set "+campos+" where id_usuario = "+body.id+" ";
        var [rows] = await mysql.query(sql);

        res.json({
            status: "ok",
            senha: senha,
            resultado: rows.affectedRows
        });
    }

}

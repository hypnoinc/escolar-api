const Aluno      = require('./aluno');
const Disciplina = require('./disciplina');
const Ensino     = require('./ensino');
const Nota       = require('./nota');
const Usuario    = require('./usuario');
const Turma      = require('./turma');

const { valid } = require('nodecaf').assertions;

module.exports = function({ post, get, del, head, patch, put }){

    post('/login', Usuario.login);

    post('/disciplina', Disciplina.get);
    post('/disciplina/salvar', Disciplina.salvar);
    post('/disciplina/excluir', Disciplina.excluir);

    post('/aluno', Aluno.get);
    post('/aluno/salvar', Aluno.salvar);
    post('/aluno/excluir', Aluno.excluir);
    post('/aluno/senha', Aluno.senha);

    post('/ensino', Ensino.get);

    post('/nota', Nota.get);
    post('/nota/salvar', Nota.salvar);
    post('/nota/excluir', Nota.excluir);

    post('/usuario', Usuario.get);
    post('/usuario/salvar', Usuario.salvar);
    post('/usuario/excluir', Usuario.excluir);

    post('/turma', Turma.get);
    post('/turma/salvar', Turma.salvar);
    post('/turma/excluir', Turma.excluir);

}

const { AppServer } = require('nodecaf');
const api = require('./api');
const mysql = require('mysql2/promise');

module.exports = function init(){
    let app = new AppServer(__dirname + '/default.toml');

    let shared = {};

    app.beforeStart = async function({ conf }){
        shared.mysql = await mysql.createPool(conf.mysql);
    };

    app.afterStop = async function(){
        shared.mysql.close();
    };

    app.expose(shared);
    app.api(api);

    return app;
}

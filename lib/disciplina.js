

module.exports = {

    async get({ mysql, res, body }){

        var err = '';

        if(!body.idEscola) err = 'Escola invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

            var campos = " disciplina.* ";
        var sql  = " SELECT "+campos+" FROM disciplina ";
        if (body.idTurma)
            sql += " INNER JOIN turma_disciplina ON turma_disciplina.id_disciplina = disciplina.id_disciplina ";
        sql += " WHERE disciplina.id_escola = '"+body.idEscola+"' ";

        if (body.id_disciplina)
            sql += " and disciplina.id_disciplina = "+body.id_disciplina+" ";

        if (body.idTurma)
            sql += " and turma_disciplina.id_turma = "+body.idTurma+" ";

        if(body.search)
            sql += " and disciplina.nome like '%"+body.search+"%' ";

        sql += " ORDER BY disciplina.nome";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            if (body.id_disciplina)
                res.json({
                    status: "ok",
                    resultado: rows[0]
                });
            else
                res.json({
                    status: "ok",
                    resultado: rows
                });
        else
            res.json({
                status: "ok",
                resultado: []
            });

    },

    async salvar({ mysql, res, body }){
        var err = '';

        if(!body.nome) err  = 'Nome invalido.';
        if(!body.sigla) err = 'Sigla invalido.';
        if(!body.cor) err   = 'Cor invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        if (body.id_disciplina) {

            var campos = " nome='"+body.nome+"', sigla='"+body.sigla+"', cor='"+body.cor+"' ";
            var sql  = " update disciplina set "+campos+" where id_disciplina = "+body.id_disciplina+" ";


        } else {

            var campos = " id_disciplina, nome, sigla, cor, id_escola ";
            var valores = " null, '"+body.nome+"', '"+body.sigla+"', '"+body.cor+"', '"+body.idEscola+"' ";
            var sql  = " insert into disciplina ("+campos+") values ("+valores+") ";

        }

        var [rows] = await mysql.query(sql);

        if (body.id_disciplina)
            res.json({
                status: "ok",
                resultado: rows.affectedRows
            });
        else
            res.json({
                status: "ok",
                resultado: rows.insertId
            });

    },

    async excluir ({ mysql, res, body }){
        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var sql  = " delete from disciplina where id_disciplina = "+body.id+" ";
        var [rows] = await mysql.query(sql);
        res.json({
            status: "ok",
            resultado: rows.affectedRows
        });
    }

}

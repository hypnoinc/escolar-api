

module.exports = {

    async get({ mysql, res, body }){

        var err = '';

        if(!body.idEscola) err = 'Escola invalida.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var campos = " nota.*,disciplina.nome as disciplina ";
        var sql  = " SELECT "+campos+" FROM nota ";
        sql += " INNER JOIN aluno_turma ON aluno_turma.id_aluno_turma = nota.id_aluno_turma ";
        sql += " LEFT  JOIN disciplina ON disciplina.id_disciplina = nota.id_disciplina ";
        sql += " LEFT  JOIN area ON area.id_area = nota.id_area ";
        sql += " WHERE 1 = 1 ";
        if (!body.id)
            sql += "   AND aluno_turma.fl_ativo = 'S' ";

        if (body.id)
            sql += " and nota.id_nota = "+body.id+" ";

        if (body.idAlunoTurma)
            sql += " and nota.id_aluno_turma = "+body.idAlunoTurma+" ";

        sql += " ORDER BY nota.periodo, disciplina ";

        var [rows] = await mysql.query(sql);

        if (rows.length > 0)
            if (body.id)
                res.json({
                    status: "ok",
                    resultado: rows[0]
                });
            else
                res.json({
                    status: "ok",
                    resultado: rows
                });
        else
            res.json({
                status: "ok",
                resultado: []
            });

    },

    async salvar({ mysql, res, body }){
        var err = '';

        if(!body.periodo) err        = 'Periodo invalido.';
        if(!body.id_disciplina) err  = 'Disciplina invalida.';
        if(!body.valor) err          = 'Valor invalido.';

        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        if (body.id_nota) {

           var campos = " valor='"+body.valor+"', conceito='"+body.conceito+"', periodo='"+body.periodo+"', id_disciplina='"+body.id_disciplina+"'";
           var sql  = " update nota set "+campos+" where id_nota = "+body.id_nota+" ";


        } else {

           var campos = " id_nota, id_aluno_turma, valor, conceito, periodo, id_disciplina ";
           var valores = " null, '"+body.id_aluno_turma+"', '"+body.valor+"', '"+body.conceito+"', '"+body.periodo+"', '"+body.id_disciplina+"' ";
           var sql  = " insert into nota ("+campos+") values ("+valores+") ";

        }

        var [rows] = await mysql.query(sql);

        if (body.id_nota)
            res.json({
                status: "ok",
                resultado: rows.affectedRows
            });
        else
            res.json({
                status: "ok",
                resultado: rows.insertId
            });

    },

    async excluir ({ mysql, res, body }){
        var err = '';
        if(!body.id) err = 'ID invalido.';
        if(err!='')
            return res.json({
                status: "fail",
                msg: err
            });

        var sql  = " delete from nota where id_nota = "+body.id+" ";
        var [rows] = await mysql.query(sql);
        res.json({
            status: "ok",
            resultado: rows.affectedRows
        });
    }

}
